create table nhap_xuat(
	ma_phieu CHAR(30),
	ten_hang CHAR(30),
	kho_giao CHAR(30),
	kho_nhan CHAR(30),
	don_vi CHAR(30),
	so_luong float,
	don_gia float,
	nguoi_nhan CHAR(50),
	nguoi_giao CHAR(50),
	tinh_chat CHAR(30),
	constraint primary key (ma_phieu, ten_hang)
);

create table nhien_lieu(
	ten CHAR(30),
	don_vi CHAR(30),
	ma_nhien_lieu CHAR(30) primary key,
	loai boolean 
);

create table kho(
	ten_kho CHAR(30) primary key,
	loai boolean,
	thu_kho CHAR(50)
);

create table xe_may(
	ten_xe CHAR(30) primary key,
	so_luong int
);

create table nhap_xuat_nhien_lieu(
	ma_phieu CHAR(30) primary key,
	ten_hang CHAR(30),
	ma_nhien_lieu CHAR(30),
	FOREIGN KEY (ma_phieu, ten_hang) REFERENCES nhap_xuat(ma_phieu, ten_hang),
	ngay_nhap date,
	don_gia float,
	FOREIGN KEY (ma_nhien_lieu) REFERENCES nhien_lieu(ma_nhien_lieu)
);

create table kho_chua_nhien_lieu(
	ma_nhien_lieu CHAR(30),
	ten_kho CHAR(30),
	FOREIGN KEY (ma_nhien_lieu) REFERENCES nhien_lieu(ma_nhien_lieu),
	so_luong float not null,
    ky_kt char,
	FOREIGN KEY (ten_kho) REFERENCES kho(ten_kho),
    so_du_dau_ky float,
    so_du_bat_dau float
);

create table nhien_lieu_dung_cho_xe(
	ten_xe CHAR(30),
	ma_nhien_lieu CHAR(30),
	FOREIGN KEY (ten_xe) REFERENCES xe_may(ten_xe),
	FOREIGN KEY (ma_nhien_lieu) REFERENCES nhien_lieu(ma_nhien_lieu),
	dinh_muc_hd_km float not null,
	dinh_muc_hd_h float not null,
	luong_hd_km float not null,
	luong_hd_h float not null,
    luong_dung float
);

ALTER TABLE nhap_xuat add constraint FOREIGN KEY (kho_giao) REFERENCES kho(ten_kho);
alter table kho_chua_nhien_lieu add primary key (ma_nhien_lieu,ten_kho,ky_kt);
alter table nhien_lieu_dung_cho_xe add primary key (ten_xe,ma_nhien_lieu);