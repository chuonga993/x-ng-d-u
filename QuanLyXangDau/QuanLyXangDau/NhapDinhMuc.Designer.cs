﻿namespace QuanLyXangDau
{
    partial class frmNhapDinhMuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtgvNhapDinhMuc = new System.Windows.Forms.DataGridView();
            this.dinhmuchdDataSet = new QuanLyXangDau.dinhmuchdDataSet();
            this.nhienlieudungchoxeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nhien_lieu_dung_cho_xeTableAdapter = new QuanLyXangDau.dinhmuchdDataSetTableAdapters.nhien_lieu_dung_cho_xeTableAdapter();
            this.tenxeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.manhienlieuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dinhmuchdkmDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dinhmuchdhDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvNhapDinhMuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dinhmuchdDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhienlieudungchoxeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvNhapDinhMuc
            // 
            this.dtgvNhapDinhMuc.AutoGenerateColumns = false;
            this.dtgvNhapDinhMuc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvNhapDinhMuc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenxeDataGridViewTextBoxColumn,
            this.manhienlieuDataGridViewTextBoxColumn,
            this.dinhmuchdkmDataGridViewTextBoxColumn,
            this.dinhmuchdhDataGridViewTextBoxColumn});
            this.dtgvNhapDinhMuc.DataSource = this.nhienlieudungchoxeBindingSource;
            this.dtgvNhapDinhMuc.Location = new System.Drawing.Point(3, 2);
            this.dtgvNhapDinhMuc.Name = "dtgvNhapDinhMuc";
            this.dtgvNhapDinhMuc.Size = new System.Drawing.Size(618, 222);
            this.dtgvNhapDinhMuc.TabIndex = 0;
            // 
            // dinhmuchdDataSet
            // 
            this.dinhmuchdDataSet.DataSetName = "dinhmuchdDataSet";
            this.dinhmuchdDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nhienlieudungchoxeBindingSource
            // 
            this.nhienlieudungchoxeBindingSource.DataMember = "nhien_lieu_dung_cho_xe";
            this.nhienlieudungchoxeBindingSource.DataSource = this.dinhmuchdDataSet;
            // 
            // nhien_lieu_dung_cho_xeTableAdapter
            // 
            this.nhien_lieu_dung_cho_xeTableAdapter.ClearBeforeFill = true;
            // 
            // tenxeDataGridViewTextBoxColumn
            // 
            this.tenxeDataGridViewTextBoxColumn.DataPropertyName = "ten_xe";
            this.tenxeDataGridViewTextBoxColumn.HeaderText = "ten_xe";
            this.tenxeDataGridViewTextBoxColumn.Name = "tenxeDataGridViewTextBoxColumn";
            // 
            // manhienlieuDataGridViewTextBoxColumn
            // 
            this.manhienlieuDataGridViewTextBoxColumn.DataPropertyName = "ma_nhien_lieu";
            this.manhienlieuDataGridViewTextBoxColumn.HeaderText = "ma_nhien_lieu";
            this.manhienlieuDataGridViewTextBoxColumn.Name = "manhienlieuDataGridViewTextBoxColumn";
            // 
            // dinhmuchdkmDataGridViewTextBoxColumn
            // 
            this.dinhmuchdkmDataGridViewTextBoxColumn.DataPropertyName = "dinh_muc_hd_km";
            this.dinhmuchdkmDataGridViewTextBoxColumn.HeaderText = "dinh_muc_hd_km";
            this.dinhmuchdkmDataGridViewTextBoxColumn.Name = "dinhmuchdkmDataGridViewTextBoxColumn";
            // 
            // dinhmuchdhDataGridViewTextBoxColumn
            // 
            this.dinhmuchdhDataGridViewTextBoxColumn.DataPropertyName = "dinh_muc_hd_h";
            this.dinhmuchdhDataGridViewTextBoxColumn.HeaderText = "dinh_muc_hd_h";
            this.dinhmuchdhDataGridViewTextBoxColumn.Name = "dinhmuchdhDataGridViewTextBoxColumn";
            // 
            // frmNhapDinhMuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 280);
            this.Controls.Add(this.dtgvNhapDinhMuc);
            this.Name = "frmNhapDinhMuc";
            this.Text = "NhapDinhMuc";
            this.Load += new System.EventHandler(this.frmNhapDinhMuc_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvNhapDinhMuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dinhmuchdDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhienlieudungchoxeBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvNhapDinhMuc;
        private dinhmuchdDataSet dinhmuchdDataSet;
        private System.Windows.Forms.BindingSource nhienlieudungchoxeBindingSource;
        private dinhmuchdDataSetTableAdapters.nhien_lieu_dung_cho_xeTableAdapter nhien_lieu_dung_cho_xeTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenxeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn manhienlieuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dinhmuchdkmDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dinhmuchdhDataGridViewTextBoxColumn;
    }
}