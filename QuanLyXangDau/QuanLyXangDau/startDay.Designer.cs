﻿namespace QuanLyXangDau
{
    partial class frmstartDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Day = new System.Windows.Forms.Label();
            this.txtbxDay = new System.Windows.Forms.TextBox();
            this.calendarDayLoad = new System.Windows.Forms.MonthCalendar();
            this.SuspendLayout();
            // 
            // Day
            // 
            this.Day.AutoSize = true;
            this.Day.Location = new System.Drawing.Point(12, 24);
            this.Day.Name = "Day";
            this.Day.Size = new System.Drawing.Size(32, 13);
            this.Day.TabIndex = 0;
            this.Day.Text = "Ngày";
            // 
            // txtbxDay
            // 
            this.txtbxDay.Location = new System.Drawing.Point(50, 21);
            this.txtbxDay.Name = "txtbxDay";
            this.txtbxDay.Size = new System.Drawing.Size(100, 20);
            this.txtbxDay.TabIndex = 1;
            // 
            // calendarDayLoad
            // 
            this.calendarDayLoad.Location = new System.Drawing.Point(173, 9);
            this.calendarDayLoad.Name = "calendarDayLoad";
            this.calendarDayLoad.TabIndex = 3;
            this.calendarDayLoad.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendarDayLoad_DateChanged);
            // 
            // frmstartDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 180);
            this.Controls.Add(this.calendarDayLoad);
            this.Controls.Add(this.txtbxDay);
            this.Controls.Add(this.Day);
            this.Name = "frmstartDay";
            this.Text = "Ngày nhập liệu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Day;
        private System.Windows.Forms.TextBox txtbxDay;
        private System.Windows.Forms.MonthCalendar calendarDayLoad;

    }
}