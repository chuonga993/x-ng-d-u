﻿namespace QuanLyXangDau
{
    partial class frmKyKT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtThang = new System.Windows.Forms.RadioButton();
            this.rbtQuy = new System.Windows.Forms.RadioButton();
            this.rbtNam = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // rbtThang
            // 
            this.rbtThang.AutoSize = true;
            this.rbtThang.Location = new System.Drawing.Point(37, 12);
            this.rbtThang.Name = "rbtThang";
            this.rbtThang.Size = new System.Drawing.Size(56, 17);
            this.rbtThang.TabIndex = 0;
            this.rbtThang.TabStop = true;
            this.rbtThang.Text = "Tháng";
            this.rbtThang.UseVisualStyleBackColor = true;
            // 
            // rbtQuy
            // 
            this.rbtQuy.AutoSize = true;
            this.rbtQuy.Location = new System.Drawing.Point(37, 44);
            this.rbtQuy.Name = "rbtQuy";
            this.rbtQuy.Size = new System.Drawing.Size(44, 17);
            this.rbtQuy.TabIndex = 1;
            this.rbtQuy.TabStop = true;
            this.rbtQuy.Text = "Quý";
            this.rbtQuy.UseVisualStyleBackColor = true;
            // 
            // rbtNam
            // 
            this.rbtNam.AutoSize = true;
            this.rbtNam.Location = new System.Drawing.Point(37, 80);
            this.rbtNam.Name = "rbtNam";
            this.rbtNam.Size = new System.Drawing.Size(47, 17);
            this.rbtNam.TabIndex = 2;
            this.rbtNam.TabStop = true;
            this.rbtNam.Text = "Năm";
            this.rbtNam.UseVisualStyleBackColor = true;
            // 
            // frmKyKT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 137);
            this.Controls.Add(this.rbtNam);
            this.Controls.Add(this.rbtQuy);
            this.Controls.Add(this.rbtThang);
            this.Name = "frmKyKT";
            this.Text = "Kỳ Kế toán";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtThang;
        private System.Windows.Forms.RadioButton rbtQuy;
        private System.Windows.Forms.RadioButton rbtNam;
    }
}