﻿namespace QuanLyXangDau
{
    partial class frmXe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtgvXe = new System.Windows.Forms.DataGridView();
            this.xemayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.xeDataSet = new QuanLyXangDau.xeDataSet();
            this.xe_mayTableAdapter = new QuanLyXangDau.xeDataSetTableAdapters.xe_mayTableAdapter();
            this.tenxeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soluongDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loai = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvXe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xemayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xeDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvXe
            // 
            this.dtgvXe.AutoGenerateColumns = false;
            this.dtgvXe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvXe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenxeDataGridViewTextBoxColumn,
            this.soluongDataGridViewTextBoxColumn,
            this.loai});
            this.dtgvXe.DataSource = this.xemayBindingSource;
            this.dtgvXe.Location = new System.Drawing.Point(0, 0);
            this.dtgvXe.Name = "dtgvXe";
            this.dtgvXe.Size = new System.Drawing.Size(623, 223);
            this.dtgvXe.TabIndex = 0;
            // 
            // xemayBindingSource
            // 
            this.xemayBindingSource.DataMember = "xe_may";
            this.xemayBindingSource.DataSource = this.xeDataSet;
            // 
            // xeDataSet
            // 
            this.xeDataSet.DataSetName = "xeDataSet";
            this.xeDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xe_mayTableAdapter
            // 
            this.xe_mayTableAdapter.ClearBeforeFill = true;
            // 
            // tenxeDataGridViewTextBoxColumn
            // 
            this.tenxeDataGridViewTextBoxColumn.DataPropertyName = "ten_xe";
            this.tenxeDataGridViewTextBoxColumn.HeaderText = "Tên xe";
            this.tenxeDataGridViewTextBoxColumn.Name = "tenxeDataGridViewTextBoxColumn";
            // 
            // soluongDataGridViewTextBoxColumn
            // 
            this.soluongDataGridViewTextBoxColumn.DataPropertyName = "so_luong";
            this.soluongDataGridViewTextBoxColumn.HeaderText = "Số lượng";
            this.soluongDataGridViewTextBoxColumn.Name = "soluongDataGridViewTextBoxColumn";
            // 
            // loai
            // 
            this.loai.DataPropertyName = "loai";
            this.loai.HeaderText = "Loại";
            this.loai.Name = "loai";
            // 
            // frmXe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 308);
            this.Controls.Add(this.dtgvXe);
            this.Name = "frmXe";
            this.Text = "Xe";
            this.Load += new System.EventHandler(this.frmXe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvXe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xemayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xeDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvXe;
        private xeDataSet xeDataSet;
        private System.Windows.Forms.BindingSource xemayBindingSource;
        private xeDataSetTableAdapters.xe_mayTableAdapter xe_mayTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenxeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn soluongDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn loai;
    }
}