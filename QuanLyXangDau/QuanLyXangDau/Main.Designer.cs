﻿namespace QuanLyXangDau
{
    partial class frmQuanlyxangdau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hệThốngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kỳKếToánToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhMụcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhiênLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.khoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.thamSốMặcĐịnhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.địnhMứcSửDụngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chỉnhSửaMẫuInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phânHệToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hàngKhôngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mặtĐấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thoátToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chứngTừNhậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chứngTừXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.khoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vàoSốDưToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thốngKêTồnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kho910ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kho920ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.khoTrungTâmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tổngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bảngNhậpXuấtTồnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoNhậpXuấtTồnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoHaoHụtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoNhậpXuấtTiềnHàngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoNhậpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoXuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoNhậpXuấtNộiBộToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dtgvMain = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvMain)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hệThốngToolStripMenuItem,
            this.nhậpXuấtToolStripMenuItem,
            this.khoToolStripMenuItem,
            this.xeToolStripMenuItem,
            this.báoCáoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(773, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hệThốngToolStripMenuItem
            // 
            this.hệThốngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem,
            this.kỳKếToánToolStripMenuItem,
            this.danhMụcToolStripMenuItem,
            this.thamSốMặcĐịnhToolStripMenuItem,
            this.chỉnhSửaMẫuInToolStripMenuItem,
            this.phânHệToolStripMenuItem,
            this.thoátToolStripMenuItem});
            this.hệThốngToolStripMenuItem.Name = "hệThốngToolStripMenuItem";
            this.hệThốngToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.hệThốngToolStripMenuItem.Text = "Hệ thống";
            this.hệThốngToolStripMenuItem.Click += new System.EventHandler(this.hệThốngToolStripMenuItem_Click);
            // 
            // ngàyBắtĐầuNhậpLiệuToolStripMenuItem
            // 
            this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem.Name = "ngàyBắtĐầuNhậpLiệuToolStripMenuItem";
            this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem.Text = "Ngày bắt đầu nhập liệu";
            this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem.Click += new System.EventHandler(this.ngàyBắtĐầuNhậpLiệuToolStripMenuItem_Click);
            // 
            // kỳKếToánToolStripMenuItem
            // 
            this.kỳKếToánToolStripMenuItem.Name = "kỳKếToánToolStripMenuItem";
            this.kỳKếToánToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.kỳKếToánToolStripMenuItem.Text = "Kỳ kế toán";
            this.kỳKếToánToolStripMenuItem.Click += new System.EventHandler(this.kỳKếToánToolStripMenuItem_Click);
            // 
            // danhMụcToolStripMenuItem
            // 
            this.danhMụcToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nhiênLiệuToolStripMenuItem,
            this.khoToolStripMenuItem1,
            this.xeToolStripMenuItem1});
            this.danhMụcToolStripMenuItem.Name = "danhMụcToolStripMenuItem";
            this.danhMụcToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.danhMụcToolStripMenuItem.Text = "Danh mục";
            // 
            // nhiênLiệuToolStripMenuItem
            // 
            this.nhiênLiệuToolStripMenuItem.Name = "nhiênLiệuToolStripMenuItem";
            this.nhiênLiệuToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.nhiênLiệuToolStripMenuItem.Text = "Nhiên liệu";
            this.nhiênLiệuToolStripMenuItem.Click += new System.EventHandler(this.nhiênLiệuToolStripMenuItem_Click);
            // 
            // khoToolStripMenuItem1
            // 
            this.khoToolStripMenuItem1.Name = "khoToolStripMenuItem1";
            this.khoToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.khoToolStripMenuItem1.Text = "Kho";
            this.khoToolStripMenuItem1.Click += new System.EventHandler(this.khoToolStripMenuItem_Click);
            // 
            // xeToolStripMenuItem1
            // 
            this.xeToolStripMenuItem1.Name = "xeToolStripMenuItem1";
            this.xeToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.xeToolStripMenuItem1.Text = "Xe";
            this.xeToolStripMenuItem1.Click += new System.EventHandler(this.xeToolStripMenuItem_Click);
            // 
            // thamSốMặcĐịnhToolStripMenuItem
            // 
            this.thamSốMặcĐịnhToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.địnhMứcSửDụngToolStripMenuItem,
            this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem});
            this.thamSốMặcĐịnhToolStripMenuItem.Name = "thamSốMặcĐịnhToolStripMenuItem";
            this.thamSốMặcĐịnhToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.thamSốMặcĐịnhToolStripMenuItem.Text = "Tham số mặc định";
            // 
            // địnhMứcSửDụngToolStripMenuItem
            // 
            this.địnhMứcSửDụngToolStripMenuItem.Name = "địnhMứcSửDụngToolStripMenuItem";
            this.địnhMứcSửDụngToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.địnhMứcSửDụngToolStripMenuItem.Text = "Định mức sử dụng";
            this.địnhMứcSửDụngToolStripMenuItem.Click += new System.EventHandler(this.địnhMứcSửDụngToolStripMenuItem_Click);
            // 
            // sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem
            // 
            this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem.Name = "sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem";
            this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem.Text = "Số dư ngày bắt đầu nhập liệu";
            this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem.Click += new System.EventHandler(this.sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem_Click);
            // 
            // chỉnhSửaMẫuInToolStripMenuItem
            // 
            this.chỉnhSửaMẫuInToolStripMenuItem.Name = "chỉnhSửaMẫuInToolStripMenuItem";
            this.chỉnhSửaMẫuInToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.chỉnhSửaMẫuInToolStripMenuItem.Text = "Chỉnh sửa mẫu in";
            // 
            // phânHệToolStripMenuItem
            // 
            this.phânHệToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hàngKhôngToolStripMenuItem,
            this.mặtĐấtToolStripMenuItem});
            this.phânHệToolStripMenuItem.Name = "phânHệToolStripMenuItem";
            this.phânHệToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.phânHệToolStripMenuItem.Text = "Phân hệ";
            // 
            // hàngKhôngToolStripMenuItem
            // 
            this.hàngKhôngToolStripMenuItem.Name = "hàngKhôngToolStripMenuItem";
            this.hàngKhôngToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.hàngKhôngToolStripMenuItem.Text = "Hàng không";
            // 
            // mặtĐấtToolStripMenuItem
            // 
            this.mặtĐấtToolStripMenuItem.Name = "mặtĐấtToolStripMenuItem";
            this.mặtĐấtToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.mặtĐấtToolStripMenuItem.Text = "Mặt đất";
            // 
            // thoátToolStripMenuItem
            // 
            this.thoátToolStripMenuItem.Name = "thoátToolStripMenuItem";
            this.thoátToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.thoátToolStripMenuItem.Text = "Thoát";
            this.thoátToolStripMenuItem.Click += new System.EventHandler(this.thoátToolStripMenuItem_Click);
            // 
            // nhậpXuấtToolStripMenuItem
            // 
            this.nhậpXuấtToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chứngTừNhậpToolStripMenuItem,
            this.chứngTừXuấtToolStripMenuItem,
            this.chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem});
            this.nhậpXuấtToolStripMenuItem.Name = "nhậpXuấtToolStripMenuItem";
            this.nhậpXuấtToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.nhậpXuấtToolStripMenuItem.Text = "Nhập xuất";
            this.nhậpXuấtToolStripMenuItem.Click += new System.EventHandler(this.nhậpXuấtToolStripMenuItem_Click);
            // 
            // chứngTừNhậpToolStripMenuItem
            // 
            this.chứngTừNhậpToolStripMenuItem.Name = "chứngTừNhậpToolStripMenuItem";
            this.chứngTừNhậpToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.chứngTừNhậpToolStripMenuItem.Text = "Chứng từ nhập";
            // 
            // chứngTừXuấtToolStripMenuItem
            // 
            this.chứngTừXuấtToolStripMenuItem.Name = "chứngTừXuấtToolStripMenuItem";
            this.chứngTừXuấtToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.chứngTừXuấtToolStripMenuItem.Text = "Chứng từ xuất";
            // 
            // chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem
            // 
            this.chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem.Name = "chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem";
            this.chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem.Text = "Chứng từ nhập xuất điều chuyển";
            // 
            // khoToolStripMenuItem
            // 
            this.khoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vàoSốDưToolStripMenuItem,
            this.thốngKêTồnToolStripMenuItem,
            this.bảngNhậpXuấtTồnToolStripMenuItem});
            this.khoToolStripMenuItem.Name = "khoToolStripMenuItem";
            this.khoToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.khoToolStripMenuItem.Text = "Kho";
            this.khoToolStripMenuItem.Click += new System.EventHandler(this.khoToolStripMenuItem_Click_1);
            // 
            // vàoSốDưToolStripMenuItem
            // 
            this.vàoSốDưToolStripMenuItem.Name = "vàoSốDưToolStripMenuItem";
            this.vàoSốDưToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.vàoSốDưToolStripMenuItem.Text = "Vào số dư";
            // 
            // thốngKêTồnToolStripMenuItem
            // 
            this.thốngKêTồnToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kho910ToolStripMenuItem,
            this.kho920ToolStripMenuItem,
            this.khoTrungTâmToolStripMenuItem,
            this.tổngToolStripMenuItem});
            this.thốngKêTồnToolStripMenuItem.Name = "thốngKêTồnToolStripMenuItem";
            this.thốngKêTồnToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.thốngKêTồnToolStripMenuItem.Text = "Thống kê tồn";
            // 
            // kho910ToolStripMenuItem
            // 
            this.kho910ToolStripMenuItem.Name = "kho910ToolStripMenuItem";
            this.kho910ToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.kho910ToolStripMenuItem.Text = "Kho 910";
            // 
            // kho920ToolStripMenuItem
            // 
            this.kho920ToolStripMenuItem.Name = "kho920ToolStripMenuItem";
            this.kho920ToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.kho920ToolStripMenuItem.Text = "Kho 920";
            // 
            // khoTrungTâmToolStripMenuItem
            // 
            this.khoTrungTâmToolStripMenuItem.Name = "khoTrungTâmToolStripMenuItem";
            this.khoTrungTâmToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.khoTrungTâmToolStripMenuItem.Text = "Kho Trung tâm";
            // 
            // tổngToolStripMenuItem
            // 
            this.tổngToolStripMenuItem.Name = "tổngToolStripMenuItem";
            this.tổngToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.tổngToolStripMenuItem.Text = "Tổng";
            // 
            // bảngNhậpXuấtTồnToolStripMenuItem
            // 
            this.bảngNhậpXuấtTồnToolStripMenuItem.Name = "bảngNhậpXuấtTồnToolStripMenuItem";
            this.bảngNhậpXuấtTồnToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.bảngNhậpXuấtTồnToolStripMenuItem.Text = "Bảng nhập xuất tồn";
            // 
            // xeToolStripMenuItem
            // 
            this.xeToolStripMenuItem.Name = "xeToolStripMenuItem";
            this.xeToolStripMenuItem.Size = new System.Drawing.Size(32, 20);
            this.xeToolStripMenuItem.Text = "Xe";
            this.xeToolStripMenuItem.Click += new System.EventHandler(this.xeToolStripMenuItem_Click_1);
            // 
            // báoCáoToolStripMenuItem
            // 
            this.báoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.báoCáoNhậpXuấtTồnToolStripMenuItem,
            this.báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem,
            this.báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem,
            this.báoCáoHaoHụtToolStripMenuItem,
            this.báoCáoNhậpXuấtTiềnHàngToolStripMenuItem,
            this.báoCáoNhậpToolStripMenuItem,
            this.báoCáoXuấtToolStripMenuItem,
            this.báoCáoNhậpXuấtNộiBộToolStripMenuItem,
            this.báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem});
            this.báoCáoToolStripMenuItem.Name = "báoCáoToolStripMenuItem";
            this.báoCáoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.báoCáoToolStripMenuItem.Text = "Báo cáo";
            // 
            // báoCáoNhậpXuấtTồnToolStripMenuItem
            // 
            this.báoCáoNhậpXuấtTồnToolStripMenuItem.Name = "báoCáoNhậpXuấtTồnToolStripMenuItem";
            this.báoCáoNhậpXuấtTồnToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoNhậpXuấtTồnToolStripMenuItem.Text = "Báo cáo nhập xuất tồn";
            // 
            // báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem
            // 
            this.báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem.Name = "báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem";
            this.báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem.Text = "Báo cáo tiêu thụ xăng dầu theo nhiệm vụ";
            // 
            // báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem
            // 
            this.báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem.Name = "báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem";
            this.báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem.Text = "Báo cáo tiêu thụ xăng dầu theo xe máy";
            // 
            // báoCáoHaoHụtToolStripMenuItem
            // 
            this.báoCáoHaoHụtToolStripMenuItem.Name = "báoCáoHaoHụtToolStripMenuItem";
            this.báoCáoHaoHụtToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoHaoHụtToolStripMenuItem.Text = "Báo cáo hao hụt";
            // 
            // báoCáoNhậpXuấtTiềnHàngToolStripMenuItem
            // 
            this.báoCáoNhậpXuấtTiềnHàngToolStripMenuItem.Name = "báoCáoNhậpXuấtTiềnHàngToolStripMenuItem";
            this.báoCáoNhậpXuấtTiềnHàngToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoNhậpXuấtTiềnHàngToolStripMenuItem.Text = "Báo cáo nhập xuất tiền hàng";
            // 
            // báoCáoNhậpToolStripMenuItem
            // 
            this.báoCáoNhậpToolStripMenuItem.Name = "báoCáoNhậpToolStripMenuItem";
            this.báoCáoNhậpToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoNhậpToolStripMenuItem.Text = "Báo cáo nhập";
            // 
            // báoCáoXuấtToolStripMenuItem
            // 
            this.báoCáoXuấtToolStripMenuItem.Name = "báoCáoXuấtToolStripMenuItem";
            this.báoCáoXuấtToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoXuấtToolStripMenuItem.Text = "Báo cáo xuất";
            // 
            // báoCáoNhậpXuấtNộiBộToolStripMenuItem
            // 
            this.báoCáoNhậpXuấtNộiBộToolStripMenuItem.Name = "báoCáoNhậpXuấtNộiBộToolStripMenuItem";
            this.báoCáoNhậpXuấtNộiBộToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoNhậpXuấtNộiBộToolStripMenuItem.Text = "Báo cáo nhập xuất nội bộ";
            // 
            // báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem
            // 
            this.báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem.Name = "báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem";
            this.báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem.Size = new System.Drawing.Size(291, 22);
            this.báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem.Text = "Báo cáo nhập xuất với bên ngoài";
            // 
            // dtgvMain
            // 
            this.dtgvMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgvMain.Location = new System.Drawing.Point(0, 24);
            this.dtgvMain.Name = "dtgvMain";
            this.dtgvMain.Size = new System.Drawing.Size(773, 451);
            this.dtgvMain.TabIndex = 1;
            // 
            // frmQuanlyxangdau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 475);
            this.Controls.Add(this.dtgvMain);
            this.Controls.Add(this.menuStrip1);
            this.Name = "frmQuanlyxangdau";
            this.Text = "Quản lý xăng dầu";
            this.Load += new System.EventHandler(this.frmQuanlyxangdau_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hệThốngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ngàyBắtĐầuNhậpLiệuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kỳKếToánToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhMụcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhiênLiệuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem khoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem xeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem thamSốMặcĐịnhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem địnhMứcSửDụngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chỉnhSửaMẫuInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phânHệToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hàngKhôngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mặtĐấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhậpXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem khoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chứngTừNhậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chứngTừXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chứngTừNhậpXuấtĐiềuChuyểnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vàoSốDưToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thốngKêTồnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kho910ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kho920ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem khoTrungTâmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tổngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bảngNhậpXuấtTồnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoNhậpXuấtTồnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoTiêuThụXăngDầuTheoNhiệmVụToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoTiêuThụXăngDầuTheoXeMáyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoHaoHụtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoNhậpXuấtTiềnHàngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoNhậpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoXuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoNhậpXuấtNộiBộToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem báoCáoNhậpXuấtVớiBênNgoàiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thoátToolStripMenuItem;
        private System.Windows.Forms.DataGridView dtgvMain;

    }
}

