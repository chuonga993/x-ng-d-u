﻿namespace QuanLyXangDau
{
    partial class frmKho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmKho));
            this.dtgvKho = new System.Windows.Forms.DataGridView();
            this.khoDataSet = new QuanLyXangDau.khoDataSet();
            this.khoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.khoTableAdapter = new QuanLyXangDau.khoDataSetTableAdapters.khoTableAdapter();
            this.tenkhoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loaiDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.thukhoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvKho
            // 
            this.dtgvKho.AutoGenerateColumns = false;
            this.dtgvKho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvKho.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenkhoDataGridViewTextBoxColumn,
            this.loaiDataGridViewCheckBoxColumn,
            this.thukhoDataGridViewTextBoxColumn});
            this.dtgvKho.DataSource = this.khoBindingSource;
            resources.ApplyResources(this.dtgvKho, "dtgvKho");
            this.dtgvKho.Name = "dtgvKho";
            this.dtgvKho.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // khoDataSet
            // 
            this.khoDataSet.DataSetName = "khoDataSet";
            this.khoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // khoBindingSource
            // 
            this.khoBindingSource.DataMember = "kho";
            this.khoBindingSource.DataSource = this.khoDataSet;
            // 
            // khoTableAdapter
            // 
            this.khoTableAdapter.ClearBeforeFill = true;
            // 
            // tenkhoDataGridViewTextBoxColumn
            // 
            this.tenkhoDataGridViewTextBoxColumn.DataPropertyName = "ten_kho";
            resources.ApplyResources(this.tenkhoDataGridViewTextBoxColumn, "tenkhoDataGridViewTextBoxColumn");
            this.tenkhoDataGridViewTextBoxColumn.Name = "tenkhoDataGridViewTextBoxColumn";
            // 
            // loaiDataGridViewCheckBoxColumn
            // 
            this.loaiDataGridViewCheckBoxColumn.DataPropertyName = "loai";
            resources.ApplyResources(this.loaiDataGridViewCheckBoxColumn, "loaiDataGridViewCheckBoxColumn");
            this.loaiDataGridViewCheckBoxColumn.Name = "loaiDataGridViewCheckBoxColumn";
            // 
            // thukhoDataGridViewTextBoxColumn
            // 
            this.thukhoDataGridViewTextBoxColumn.DataPropertyName = "thu_kho";
            resources.ApplyResources(this.thukhoDataGridViewTextBoxColumn, "thukhoDataGridViewTextBoxColumn");
            this.thukhoDataGridViewTextBoxColumn.Name = "thukhoDataGridViewTextBoxColumn";
            // 
            // frmKho
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dtgvKho);
            this.Name = "frmKho";
            this.Load += new System.EventHandler(this.frmKho_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.khoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvKho;
        private khoDataSet khoDataSet;
        private System.Windows.Forms.BindingSource khoBindingSource;
        private khoDataSetTableAdapters.khoTableAdapter khoTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenkhoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn loaiDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn thukhoDataGridViewTextBoxColumn;
    }
}