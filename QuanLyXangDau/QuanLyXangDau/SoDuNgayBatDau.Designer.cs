﻿namespace QuanLyXangDau
{
    partial class frmSoDuNgayBatDau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtgvSoDuNgayBD = new System.Windows.Forms.DataGridView();
            this.sodungaybatdaunhaplieuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sodubdDataSet = new QuanLyXangDau.sodubdDataSet();
            this.so_du_ngay_bat_dau_nhap_lieuTableAdapter = new QuanLyXangDau.sodubdDataSetTableAdapters.so_du_ngay_bat_dau_nhap_lieuTableAdapter();
            this.manhienlieuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenkhoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kyktDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soluongDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sodubatdauDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sodudaukyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvSoDuNgayBD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sodungaybatdaunhaplieuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sodubdDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvSoDuNgayBD
            // 
            this.dtgvSoDuNgayBD.AutoGenerateColumns = false;
            this.dtgvSoDuNgayBD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvSoDuNgayBD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.manhienlieuDataGridViewTextBoxColumn,
            this.tenkhoDataGridViewTextBoxColumn,
            this.kyktDataGridViewTextBoxColumn,
            this.soluongDataGridViewTextBoxColumn,
            this.sodubatdauDataGridViewTextBoxColumn,
            this.sodudaukyDataGridViewTextBoxColumn});
            this.dtgvSoDuNgayBD.DataSource = this.sodungaybatdaunhaplieuBindingSource;
            this.dtgvSoDuNgayBD.Location = new System.Drawing.Point(3, 3);
            this.dtgvSoDuNgayBD.Name = "dtgvSoDuNgayBD";
            this.dtgvSoDuNgayBD.Size = new System.Drawing.Size(646, 227);
            this.dtgvSoDuNgayBD.TabIndex = 0;
            // 
            // sodungaybatdaunhaplieuBindingSource
            // 
            this.sodungaybatdaunhaplieuBindingSource.DataMember = "so_du_ngay_bat_dau_nhap_lieu";
            this.sodungaybatdaunhaplieuBindingSource.DataSource = this.sodubdDataSet;
            // 
            // sodubdDataSet
            // 
            this.sodubdDataSet.DataSetName = "sodubdDataSet";
            this.sodubdDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // so_du_ngay_bat_dau_nhap_lieuTableAdapter
            // 
            this.so_du_ngay_bat_dau_nhap_lieuTableAdapter.ClearBeforeFill = true;
            // 
            // manhienlieuDataGridViewTextBoxColumn
            // 
            this.manhienlieuDataGridViewTextBoxColumn.DataPropertyName = "ma_nhien_lieu";
            this.manhienlieuDataGridViewTextBoxColumn.HeaderText = "Nhiên liệu";
            this.manhienlieuDataGridViewTextBoxColumn.Name = "manhienlieuDataGridViewTextBoxColumn";
            // 
            // tenkhoDataGridViewTextBoxColumn
            // 
            this.tenkhoDataGridViewTextBoxColumn.DataPropertyName = "ten_kho";
            this.tenkhoDataGridViewTextBoxColumn.HeaderText = "Kho";
            this.tenkhoDataGridViewTextBoxColumn.Name = "tenkhoDataGridViewTextBoxColumn";
            // 
            // kyktDataGridViewTextBoxColumn
            // 
            this.kyktDataGridViewTextBoxColumn.DataPropertyName = "ky_kt";
            this.kyktDataGridViewTextBoxColumn.HeaderText = "Kỳ kế toán";
            this.kyktDataGridViewTextBoxColumn.Name = "kyktDataGridViewTextBoxColumn";
            // 
            // soluongDataGridViewTextBoxColumn
            // 
            this.soluongDataGridViewTextBoxColumn.DataPropertyName = "so_luong";
            this.soluongDataGridViewTextBoxColumn.HeaderText = "Số lượng";
            this.soluongDataGridViewTextBoxColumn.Name = "soluongDataGridViewTextBoxColumn";
            // 
            // sodubatdauDataGridViewTextBoxColumn
            // 
            this.sodubatdauDataGridViewTextBoxColumn.DataPropertyName = "so_du_bat_dau";
            this.sodubatdauDataGridViewTextBoxColumn.HeaderText = "Số dư ban đầu";
            this.sodubatdauDataGridViewTextBoxColumn.Name = "sodubatdauDataGridViewTextBoxColumn";
            // 
            // sodudaukyDataGridViewTextBoxColumn
            // 
            this.sodudaukyDataGridViewTextBoxColumn.DataPropertyName = "so_du_dau_ky";
            this.sodudaukyDataGridViewTextBoxColumn.HeaderText = "Số dư đầu kỳ";
            this.sodudaukyDataGridViewTextBoxColumn.Name = "sodudaukyDataGridViewTextBoxColumn";
            // 
            // frmSoDuNgayBatDau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 300);
            this.Controls.Add(this.dtgvSoDuNgayBD);
            this.Name = "frmSoDuNgayBatDau";
            this.Text = "SoDuNgayBatDau";
            this.Load += new System.EventHandler(this.frmSoDuNgayBatDau_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvSoDuNgayBD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sodungaybatdaunhaplieuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sodubdDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvSoDuNgayBD;
        private sodubdDataSet sodubdDataSet;
        private System.Windows.Forms.BindingSource sodungaybatdaunhaplieuBindingSource;
        private sodubdDataSetTableAdapters.so_du_ngay_bat_dau_nhap_lieuTableAdapter so_du_ngay_bat_dau_nhap_lieuTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn manhienlieuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenkhoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kyktDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn soluongDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sodubatdauDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sodudaukyDataGridViewTextBoxColumn;
    }
}