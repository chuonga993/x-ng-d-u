﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyXangDau
{
    public partial class frmNhapDinhMuc : Form
    {
        public frmNhapDinhMuc()
        {
            InitializeComponent();
        }

        private void frmNhapDinhMuc_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dinhmuchdDataSet.nhien_lieu_dung_cho_xe' table. You can move, or remove it, as needed.
            this.nhien_lieu_dung_cho_xeTableAdapter.Fill(this.dinhmuchdDataSet.nhien_lieu_dung_cho_xe);

        }
    }
}
