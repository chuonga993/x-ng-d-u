﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyXangDau
{
    public partial class frmstartDay : Form
    {
        public frmstartDay()
        {
            InitializeComponent();
        }

        private void btDayLoad_Click(object sender, EventArgs e)
        {
            calendarDayLoad.Visible = true;
        }

        private void calendarDayLoad_DateChanged(object sender, DateRangeEventArgs e)
        {
            txtbxDay.Text = calendarDayLoad.SelectionRange.Start.Date.ToShortDateString();
        }
    }
}
