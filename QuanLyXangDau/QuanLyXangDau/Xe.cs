﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyXangDau
{
    public partial class frmXe : Form
    {
        public frmXe()
        {
            InitializeComponent();
        }

        private void frmXe_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'xeDataSet.xe_may' table. You can move, or remove it, as needed.
            this.xe_mayTableAdapter.Fill(this.xeDataSet.xe_may);

        }
    }
}
