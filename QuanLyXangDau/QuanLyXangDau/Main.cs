﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace QuanLyXangDau
{
    public partial class frmQuanlyxangdau : Form
    {
        public frmQuanlyxangdau()
        {
            InitializeComponent();
            
        }

        private void treeHeThong_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ngàyBắtĐầuNhậpLiệuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Form startDay = new Form();
            frmstartDay std = new frmstartDay();
            std.Show();
        }

        private void frmQuanlyxangdau_Load(object sender, EventArgs e)
        {
            MySqlConnection conn;
            string ConnectionString = "server=127.0.0.1;port=3306;uid=root;pwd=nguyenquanghieu;database=xang_dau;";
            try
            {
                conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = ConnectionString;
                conn.Open();

            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void kho910ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void thốngKêTồnToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void hệThốngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void kỳKếToánToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmKyKT kykt = new frmKyKT();
            kykt.Show();
        }

        private void nhiênLiệuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNhienLieu nhienlieu = new frmNhienLieu();
            nhienlieu.Show();
        }

        private void khoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmKho Kho = new frmKho();
            Kho.Show();
        }

        private void xeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmXe Xe = new frmXe();
            Xe.Show();
        }

        private void sốDưNgàyBắtĐầuNhậpLiệuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSoDuNgayBatDau SoDuBD = new frmSoDuNgayBatDau();
            SoDuBD.Show();
        }

        private void địnhMứcSửDụngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNhapDinhMuc DMSD = new frmNhapDinhMuc();
            DMSD.Show();
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void nhậpXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string connectionString = "server=127.0.0.1;port=3306;uid=root;pwd=nguyenquanghieu;database=xang_dau;";
            string sql = "SELECT * FROM nhap_xuat";
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(sql, connection);
            connection.Open();
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "nhap_xuat"); //loi
            connection.Close();
            dtgvMain.DataSource = ds;
            dtgvMain.DataMember = "nhap_xuat";
        }

        private void xeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string connectionString = "server=127.0.0.1;port=3306;uid=root;pwd=nguyenquanghieu;database=xang_dau;";
            string sql = "SELECT * FROM xe_may";
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(sql, connection);
            connection.Open();
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "xe_may"); //loi
            connection.Close();
            dtgvMain.DataSource = ds;
            dtgvMain.DataMember = "xe_may";
        }

        private void khoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string connectionString = "server=127.0.0.1;port=3306;uid=root;pwd=nguyenquanghieu;database=xang_dau;";
            string sql = "SELECT * FROM kho";
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(sql, connection);
            connection.Open();
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "kho");
            connection.Close();
            dtgvMain.DataSource = ds;
            dtgvMain.DataMember = "kho";
        }

    }
}
