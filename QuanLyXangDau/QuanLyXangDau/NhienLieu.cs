﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace QuanLyXangDau
{
    public partial class frmNhienLieu : Form
    {
        public frmNhienLieu()
        {
            InitializeComponent();
        }

        private void frmNhienLieu_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'xang_dauDataSet.nhien_lieu' table. You can move, or remove it, as needed.
            string connectionString = "server=127.0.0.1;port=3306;uid=root;pwd=nguyenquanghieu;database=xang_dau;";
            string sql = "SELECT * FROM nhien_lieu";
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(sql, connection);
            connection.Open();
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds,"nhien_lieu"); //loi
            connection.Close();
            dtgvNhienlieu.DataSource = ds;
            dtgvNhienlieu.DataMember = "nhien_lieu";
        }
    }
}
