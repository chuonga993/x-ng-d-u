﻿namespace QuanLyXangDau
{
    partial class frmNhienLieu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtgvNhienlieu = new System.Windows.Forms.DataGridView();
            this.tenDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.manhienlieuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.donviDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loaiDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.nhienlieuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.xang_dauDataSet = new QuanLyXangDau.xang_dauDataSet();
            this.nhien_lieuTableAdapter = new QuanLyXangDau.xang_dauDataSetTableAdapters.nhien_lieuTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvNhienlieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhienlieuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xang_dauDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgvNhienlieu
            // 
            this.dtgvNhienlieu.AutoGenerateColumns = false;
            this.dtgvNhienlieu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvNhienlieu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tenDataGridViewTextBoxColumn,
            this.manhienlieuDataGridViewTextBoxColumn,
            this.donviDataGridViewTextBoxColumn,
            this.loaiDataGridViewCheckBoxColumn});
            this.dtgvNhienlieu.DataSource = this.nhienlieuBindingSource;
            this.dtgvNhienlieu.Location = new System.Drawing.Point(0, 1);
            this.dtgvNhienlieu.Name = "dtgvNhienlieu";
            this.dtgvNhienlieu.Size = new System.Drawing.Size(623, 223);
            this.dtgvNhienlieu.TabIndex = 0;
            // 
            // tenDataGridViewTextBoxColumn
            // 
            this.tenDataGridViewTextBoxColumn.DataPropertyName = "ten";
            this.tenDataGridViewTextBoxColumn.HeaderText = "Tên Nhiên liệu";
            this.tenDataGridViewTextBoxColumn.Name = "tenDataGridViewTextBoxColumn";
            // 
            // manhienlieuDataGridViewTextBoxColumn
            // 
            this.manhienlieuDataGridViewTextBoxColumn.DataPropertyName = "ma_nhien_lieu";
            this.manhienlieuDataGridViewTextBoxColumn.HeaderText = "Mã nhiên liệu";
            this.manhienlieuDataGridViewTextBoxColumn.Name = "manhienlieuDataGridViewTextBoxColumn";
            // 
            // donviDataGridViewTextBoxColumn
            // 
            this.donviDataGridViewTextBoxColumn.DataPropertyName = "don_vi";
            this.donviDataGridViewTextBoxColumn.HeaderText = "Đơn vị tính";
            this.donviDataGridViewTextBoxColumn.Name = "donviDataGridViewTextBoxColumn";
            // 
            // loaiDataGridViewCheckBoxColumn
            // 
            this.loaiDataGridViewCheckBoxColumn.DataPropertyName = "loai";
            this.loaiDataGridViewCheckBoxColumn.HeaderText = "Loại";
            this.loaiDataGridViewCheckBoxColumn.Name = "loaiDataGridViewCheckBoxColumn";
            // 
            // nhienlieuBindingSource
            // 
            this.nhienlieuBindingSource.DataMember = "nhien_lieu";
            this.nhienlieuBindingSource.DataSource = this.xang_dauDataSet;
            // 
            // xang_dauDataSet
            // 
            this.xang_dauDataSet.DataSetName = "xang_dauDataSet";
            this.xang_dauDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nhien_lieuTableAdapter
            // 
            this.nhien_lieuTableAdapter.ClearBeforeFill = true;
            // 
            // frmNhienLieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 308);
            this.Controls.Add(this.dtgvNhienlieu);
            this.Name = "frmNhienLieu";
            this.Text = "Nhiên liệu";
            this.Load += new System.EventHandler(this.frmNhienLieu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgvNhienlieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nhienlieuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xang_dauDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtgvNhienlieu;
        private xang_dauDataSet xang_dauDataSet;
        private System.Windows.Forms.BindingSource nhienlieuBindingSource;
        private xang_dauDataSetTableAdapters.nhien_lieuTableAdapter nhien_lieuTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn manhienlieuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn donviDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn loaiDataGridViewCheckBoxColumn;
    }
}